<?php

require_once __DIR__ . '/simpletest/simpletest/unit_tester.php';
require_once __DIR__ . '/simpletest/simpletest/web_tester.php';

const RESULT_PATTERN = "\nRESULT: %s of %s POINTS\n";

function getSelectedTestMethodNames($class) {
    $methodNames = getAllTestMethodNames($class);

    return array_filter($methodNames, function ($each) {
        return preg_match('/^_/', $each);
    });
}

function getAllTestMethodNames($class) {
    //$class = get_class($this);

    $r = new ReflectionClass($class);

    $testMethods = array_filter($r->getMethods(), function ($each) use ($class) {
        return $each->class === $class && $each->isPublic();
    });

    return array_map(
        function ($each) {
            return $each->name;
        }, $testMethods);
}

class UnitTestCaseExtended extends UnitTestCase {
    public function getTests() {
        return empty(getSelectedTestMethodNames(get_class($this)))
            ? getAllTestMethodNames(get_class($this))
            : getSelectedTestMethodNames(get_class($this));
    }

}

class WebTestCaseExtended extends WebTestCase {

    public function getTests() {
        return empty(getSelectedTestMethodNames(get_class($this)))
            ? getAllTestMethodNames(get_class($this))
            : getSelectedTestMethodNames(get_class($this));
    }

    public function assertFrontControllerLink($linkId) {

        $href = $this->getBrowser()->getLinkHrefById($linkId);

        $pattern = '/^(index\.php)?\?[-=&\w]*$/';

        $message = 'Front Controller pattern expects all links '
            . 'to be in ?key1=value1&key2=... format. But this link was: ' . $href;

        $this->assert(new PatternExpectation($pattern), $href, $message);
    }

    public function assertAttribute($attribute, $value) {
        $pattern = '/' . $attribute . '\s*=\s*["\']' . $value . '["\']/';

        $this->assertPattern($pattern,
            "can't find element with attribute '$attribute' and value '$value'");
    }

    public function assertNoField($fieldName) {
        $value = $this->getBrowser()->getField($fieldName);

        if ($value !== NULL) {
            $this->assertTrue(false, "field '$fieldName' should not exist");
        }
    }

    public function getLinkLabelById($id) {
        return $this->getBrowser()->getLinkLabelById($id);
    }

    public function getFieldValue($name) {
        return $this->getBrowser()->getField($name);
    }

    public function getQueryString() {
        $url = $this->getUrl();

        return preg_replace("/.*\?/", "?", $url);
    }

    function assertCurrentUrlEndsWith($expectedPath) {
        $responseCode = $this->getBrowser()->getResponseCode();
        $url = $this->normalizeUrl($this->getBrowser()->getUrl());

        if ($responseCode !== 200) {
            $this->fail("Response code $responseCode for link $url");
            return;
        }

        $this->assertTrue($this->endsWith($url, $expectedPath),
            "Expected url to end with '$expectedPath' but it was '$url'");
    }

    function ensureRelativeLink($LinkLabel) {
        $href = $this->getBrowser()->getLinkHrefByLabel($LinkLabel);

        if (preg_match("/:/", $href) || preg_match("/^\//", $href)) {
            throw new RuntimeException("$href is not a relative link");
        }

    }

    private function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private function normalizeUrl($url) {

        $hostRegex = '/https?:\/\/\w+(:\d+)?\/?/';

        preg_match($hostRegex, $url, $matches);

        $hostPart = '';
        if (isset($matches[0])) {
            $hostPart = $matches[0];
            $url = preg_replace($hostRegex, "", $url);
        }

        $parts = [];
        foreach (explode('/', $url) as $part) {
            if ($part === '.') {
                continue;
            } else if ($part === '..') {
                array_pop($parts);
            } else {
                array_push($parts, $part);
            }
        }

        return $hostPart . implode('/', $parts);
    }

}

class PointsReporter extends TextReporter {

    private $maxPoints;
    private $failCountBeforeTest;
    private $passedMethodCount = 0;
    private $totalMethodRunCount = 0;
    private $scale;

    public function __construct($maxPoints, $scale) {
        parent::__construct();
        $this->maxPoints = $maxPoints;
        $this->scale = $scale;
    }


    public function paintMethodStart($test_name) {
        $this->failCountBeforeTest =
            $this->getFailCount() + $this->getExceptionCount();

        parent::paintMethodStart($test_name);
    }

    public function paintMethodEnd($test_name) {
        $this->totalMethodRunCount++;

        if ($this->failCountBeforeTest < $this->getFailCount() + $this->getExceptionCount()) {
            printf("%s failed\n", $test_name);
        } else {
            $this->passedMethodCount++;
            printf("%s ok\n", $test_name);
        }

        parent::paintMethodEnd($test_name);
    }

    public function paintFooter($test_name) {
        $keys = array_keys($this->scale);
        $maxThreshold = array_pop($keys);

        if ($this->totalMethodRunCount < $maxThreshold) {
            return; // do not show points if some test are disabled
        }

        $finalPoints = 0;

        foreach ($this->scale as $threshold => $points) {
            if ($this->passedMethodCount >= $threshold) {
                $finalPoints = $points;
            }
        }

        printf(RESULT_PATTERN, $finalPoints, $this->maxPoints);
    }
}
